from flask import Flask
from flask import render_template
from flask import abort
from flask import request

app = Flask(__name__)

#online sourcess that helped with this project
#https://pythonise.com/series/learning-flask/flask-error-handling
#https://www3.ntu.edu.sg/home/ehchua/programming/webprogramming/Python3_Flask.html

@app.route("/")
@app.route("/<path:name>")
def get_page(name = "default"):
    """
    :param name is a path to a file such as index.html, has a default value
    :return the page requested or the correct error
    """

    #shoutout to piazza
    #get the raw url the user entered
    raw_url = request.environ['RAW_URI']

    if name == "default":
        return "UOCIS docker demo!"

    forbidden_characters = ["..", "//","~"]

    #loop to check for whether there are forbidden characters
    for character in forbidden_characters:
        if character in raw_url:
            abort(403)

    #try to return the page, if not abort with a 404 error
    try:
        return render_template(name), 200
    except Exception as e:
        abort(404)

@app.errorhandler(403)
def forbidden_page(e):
    """
    :param e is the error, in this case a 403
    :return a 403 error webpage
    """
    return render_template("403.html"), 403

@app.errorhandler(404)
def page_not_found(e):
    """
    :param e is the error, in this case a 404
    :return a 404 error webpage
    """
    return render_template("404.html"), 404


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
